package com.sergejs.vehicles;

import com.sergejs.builders.VehicleModel;
import com.sergejs.components.Body;
import com.sergejs.components.Engine;
import com.sergejs.components.Transmission;

public class VehicleImpl implements Vehicle {
    private final VehicleModel model;
    private final Body body;
    private final Engine engine;
    private final Transmission transmission;

    public VehicleImpl(VehicleModel model, Body body, Engine engine, Transmission transmission) {
        this.model = model;
        this.body = body;
        this.engine = engine;
        this.transmission = transmission;
    }

    @Override
    public String retrieveInfo() {
        return "Vehicle: " + model +
                "\n - body: " + body +
                "\n" + engine.toString() +
                "\n - Transmission: " + transmission;
    }

}
