package com.sergejs.vehicles;

public class InteriorDecorator extends VehicleDecorator {
    private String interior;
    public InteriorDecorator(Vehicle vehicle, String interior) {
        super(vehicle);
        this.interior = interior;
    }

    @Override
    public String retrieveInfo() {
        return super.retrieveInfo() + interiorInfo();
    }

    private String interiorInfo() {
        return "\n - Interior: " + interior;
    }
}
