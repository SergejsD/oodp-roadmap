package com.sergejs.vehicles;

public interface Vehicle {

    String retrieveInfo();
}
