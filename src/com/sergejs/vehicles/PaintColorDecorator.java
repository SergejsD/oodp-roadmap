package com.sergejs.vehicles;

import com.sergejs.components.Color;

public class PaintColorDecorator extends VehicleDecorator {

    private Color color;

    public PaintColorDecorator(Vehicle vehicle, Color color) {
        super(vehicle);
        this.color = color;
    }

    @Override
    public String retrieveInfo() {
        return super.retrieveInfo() + paintColor();
    }

    private String paintColor() {
        return "\n - Color:" + color;
    }
}
