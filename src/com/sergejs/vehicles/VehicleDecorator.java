package com.sergejs.vehicles;

public class VehicleDecorator implements Vehicle {

    private Vehicle vehicle;

    public VehicleDecorator(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public String retrieveInfo() {
        return vehicle.retrieveInfo();
    }
}
