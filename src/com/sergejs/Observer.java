package com.sergejs;

public interface Observer {
    void update();
}
