package com.sergejs;

public class DealerShop implements Observer {
    private String dealerShopName;

    public DealerShop(String name) {
        this.dealerShopName = name;
    }

    @Override
    public void update() {
        System.out.println(dealerShopName + "\t notification received, sending some one to buy vehicle");
    }

    @Override
    public String toString() {
        return dealerShopName;
    }
}
