package com.sergejs;

import com.sergejs.builders.VehicleModel;
import com.sergejs.components.Color;

public class Main {


    public static void main(String[] args) {
        VehicleFactoryFacade factory = new VehicleFactoryFacade();

        DealerShop dealerShopA = new DealerShop("dealer_a");
        factory.addSubscriber(dealerShopA, VehicleModel.MODEL_A);

        DealerShop dealerShopB = new DealerShop("dealer_b");
        factory.addSubscriber(dealerShopB, VehicleModel.MODEL_B);

        DealerShop dealerShopAB = new DealerShop("dealer_a/b");
        factory.addSubscriber(dealerShopAB, VehicleModel.MODEL_A);
        factory.addSubscriber(dealerShopAB, VehicleModel.MODEL_B);

        System.out.println(factory.buildModelABasic(Color.WHITE));
        System.out.println();
        System.out.println(factory.buildModelASport(Color.RED));
        System.out.println();
        System.out.println(factory.buildModelBLuxury(Color.BLACK));
        System.out.println();

        factory.removeSubscriber(dealerShopB, VehicleModel.MODEL_B);
        System.out.println(factory.buildModelBLuxury(Color.BLACK));
    }
}
