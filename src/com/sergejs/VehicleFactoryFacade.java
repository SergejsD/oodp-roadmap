package com.sergejs;

import com.sergejs.builders.ModelABuilder;
import com.sergejs.builders.ModelBBuilder;
import com.sergejs.builders.VehicleModel;
import com.sergejs.components.Color;
import com.sergejs.director.DirectorBridge;
import com.sergejs.vehicles.InteriorDecorator;
import com.sergejs.vehicles.PaintColorDecorator;
import com.sergejs.vehicles.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class VehicleFactoryFacade implements Observable{

    private List<Observer> modelAObservers = new ArrayList<>();
    private List<Observer> modelBObservers = new ArrayList<>();
    private DirectorBridge directorBridge = new DirectorBridge();
    private ModelABuilder modelABuilder = new ModelABuilder();
    private ModelBBuilder modelBBuilder = new ModelBBuilder();

    public String buildModelABasic(Color color) {
        directorBridge.constructPassengerVehicle(modelABuilder);
        Vehicle vehicle = new PaintColorDecorator(
                new InteriorDecorator(modelABuilder.getResult(), "Basic Seats, Gray Cloth"),
                color);
        modelABuilder.reset();
        notifyModelASubscribers();
        return vehicle.retrieveInfo();
    }

    public String buildModelASport(Color color) {
        directorBridge.constructSportsVehicle(modelABuilder);
        Vehicle vehicle = new PaintColorDecorator(
                new InteriorDecorator(modelABuilder.getResult(), "Sport Seats, Black/Red Cloth"),
                color);
        modelABuilder.reset();
        notifyModelASubscribers();
        return vehicle.retrieveInfo();
    }

    public String buildModelBLuxury(Color color) {
        directorBridge.constructPassengerLuxVehicle(modelBBuilder);
        Vehicle vehicle = new PaintColorDecorator(
                new InteriorDecorator(modelBBuilder.getResult(), "Comfort Seats, Beige Leather"),
                color);
        modelBBuilder.reset();
        notifyModelBSubscribers();
        return vehicle.retrieveInfo();
    }

    @Override
    public void addSubscriber(Observer observer, VehicleModel interestModel) {
        if (VehicleModel.MODEL_A.equals(interestModel)) {
            modelAObservers.add(observer);
        } else if (VehicleModel.MODEL_B.equals(interestModel)) {
            modelBObservers.add(observer);
        }
    }

    @Override
    public void removeSubscriber(Observer observer, VehicleModel interestModel) {
        System.out.println("unsubscribing " + observer);
        if (VehicleModel.MODEL_A.equals(interestModel)) {
            modelAObservers.remove(observer);
        } else if (VehicleModel.MODEL_B.equals(interestModel)) {
            modelBObservers.remove(observer);
        }
    }

    private void notifyModelASubscribers() {
        notifySubscribers(modelAObservers);
    }

    private void notifyModelBSubscribers() {
        notifySubscribers(modelBObservers);
    }

    @Override
    public void notifySubscribers(List<Observer> observers) {
        System.out.println("notifying subscribed dealers");
        observers.forEach(Observer::update);
    }
}
