package com.sergejs.builders;

import com.sergejs.components.Engine;
import com.sergejs.components.FuelType;

public class BuilderUtils {

    private BuilderUtils() {
    }

    public static Engine createEngineA() {
        return new Engine(2.2, 4, FuelType.DIESEL, 1.6);
    }

    public static Engine createEngineB() {
        return new Engine(1.6, 4, FuelType.PETROL, 2.0);
    }

    public static Engine createEngineC() {
        return new Engine(3.0, 6, FuelType.PETROL, 1.8);
    }

}