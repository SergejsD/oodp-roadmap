package com.sergejs.builders;

import com.sergejs.components.Body;
import com.sergejs.components.Engine;
import com.sergejs.components.Transmission;
import com.sergejs.vehicles.VehicleImpl;

public class ModelBBuilder implements Builder {

    private Body body;
    private Engine engine;
    private Transmission transmission;

    @Override
    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public VehicleImpl getResult() {
        return new VehicleImpl(VehicleModel.MODEL_B, body, engine, transmission);
    }
}
