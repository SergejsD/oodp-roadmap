package com.sergejs.builders;

import com.sergejs.components.Body;
import com.sergejs.components.Engine;
import com.sergejs.components.Transmission;

public interface Builder {
    void setBody(Body body);

    void setEngine(Engine engine);

    void setTransmission(Transmission transmission);

    default void reset() {
        setBody(null);
        setEngine(null);
        setTransmission(null);
    }

}
