package com.sergejs;

import com.sergejs.builders.VehicleModel;

import java.util.List;

public interface Observable {

    void addSubscriber(Observer observer, VehicleModel interestModel);

    void removeSubscriber(Observer observer, VehicleModel interestModel);

    void notifySubscribers(List<Observer> observers);
}
