package com.sergejs.components;

public enum FuelType {
    PETROL,
    DIESEL
}
