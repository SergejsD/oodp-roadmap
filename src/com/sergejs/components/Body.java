package com.sergejs.components;

public enum Body {
    COUPE,
    SEDAN,
    TOURING,
    VAN,
    SUV
}
