package com.sergejs.components;

public class Engine {
    private final double displacement;
    private double fuelConsumption;
    private int power;
    private FuelType fuelType;

    public Engine(double displacement, int cylinderCount, FuelType fuelType, double engineringCoeficent) {
        this.displacement = displacement;
        this.fuelType = fuelType;

        double basePower = (double) (((int) displacement * 1000) ^ cylinderCount) / 100 * engineringCoeficent * 3;
        double baseConsumption = displacement / cylinderCount * 10;
        if (FuelType.PETROL.equals(fuelType)) {
            this.power = (int) (basePower * 1.5);
            this.fuelConsumption = baseConsumption * 1.7;
        } else {
            this.power = (int) (basePower * 1.1);
            this.fuelConsumption = baseConsumption * 1.1;
        }
    }


    public String toString() {
        return " - Engine:" +
                "\n\t * Fuel type: " + fuelType +
                "\n\t * displacement: " + displacement + " L" +
                "\n\t * Power: " + power + " Hp" +
                "\n\t * fuelconsumption: " + fuelConsumption + " l/100km";
    }
}
