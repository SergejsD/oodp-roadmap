package com.sergejs.components;

public enum Transmission {
    MANUAL, AUTOMATIC
}
