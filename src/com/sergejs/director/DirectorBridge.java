package com.sergejs.director;

import com.sergejs.builders.Builder;
import com.sergejs.builders.BuilderUtils;
import com.sergejs.components.Body;
import com.sergejs.components.Transmission;

public class DirectorBridge {

    public void constructSportsVehicle(Builder builder) {
        builder.setEngine(BuilderUtils.createEngineC());
        builder.setTransmission(Transmission.MANUAL);
        builder.setBody(Body.COUPE);

    }

    public void constructPassengerVehicle(Builder builder) {
        builder.setEngine(BuilderUtils.createEngineB());
        builder.setTransmission(Transmission.AUTOMATIC);
        builder.setBody(Body.TOURING);
    }

    public void constructPassengerLuxVehicle(Builder builder) {
        builder.setEngine(BuilderUtils.createEngineA());
        builder.setTransmission(Transmission.AUTOMATIC);
        builder.setBody(Body.SEDAN);
    }
}
